# air_handler: when a building is created it will be given an air handler
# hvac_systems holds all instances of air_handlers

class AirHandler:
    def __init__(self, model, max_cfm, amps):
        self.model = model
        self.max_cfm = max_cfm
        self.req_amp = amps

    def to_string(self):
        s = '"' + self.model + '", cfm:' + self.max_cfm + ', amperage:' + self.amps
        return s

    def get_max_cfm(self):
        return self.max_cfm

    def __eq__(self, other):
        return self.model == other.model
