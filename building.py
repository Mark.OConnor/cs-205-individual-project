

class Building:
    def __init__(self, name):
        self.name = name
        self.building_rooms = list()
        self.air_handler = list()

    def to_string(self):
        s = "Name: " + self.name
        return s

    def __eq__(self, other):
        return self.name == other.name

    def add_room(self, room):
        while room.get_square_footage() == 0:
            square_footage = input("Room cannot be added without a size. \n"
                                   "Please enter the square footage of the room: \n")
            room.set_square_footage(square_footage)
        self.building_rooms.append(room)

    def remove_room(self, room):
        is_there = False
        for r in self.building_rooms:
            if self.building_rooms[r] == room:
                is_there = True
                self.building_rooms.remove(room)
        if not is_there:
            print("This room is not in the building, thus cannot be removed \n")

    def add_air_handler(self, air_handler):
        self.air_handler.append(air_handler)

    def remove_air_handler(self, air_handler):
        is_there = False
        for a in self.air_handler:
            if self.air_handler[a] == air_handler:
                is_there = True
                self.air_handler.remove(air_handler)
        if not is_there:
            print("This air handler is not in the building, thus cannot be removed \n")

    def calc_active_cfm(self):
        cfm = 0
        for a in self.air_handler:
            cfm = cfm + a.get_max_cfm()
        return cfm

    def calc_req_cfm(self):
        req_cfm = 0
        for r in self.building_rooms:
            req_cfm = req_cfm + r.calc_req_cfm()
        return req_cfm

    def simulate_building(self):
        cfm_diff = self.calc_active_cfm() - self.calc_req_cfm()
        if cfm_diff >= 0:
            print("The air handlers satisfy the required cfm for the " + self.name +
                  " building by " + cfm_diff + " cfm\n")
        else:
            print("The air handlers do not meet the required cfm for the " + self.name +
                  " building by " + (cfm_diff+cfm_diff*2) + " cfm\n")
        print("These are the current air_handlers in use:\n")
        for a in self.air_handler:
            print(a.to_string() + "\n")

