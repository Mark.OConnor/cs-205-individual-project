import unittest
import hvac_bank
import room
import air_handler


class TestBuilding(unittest.TestCase):
    hvac_bank = None

    def setUpClass(cls):
        print('setUpClass()')
        cls.hvac_bank = hvac_bank.HvacBank.get()

        cls.art_room = room.Room('Art', 2, 0.12, 400)
        cls.class_room = room.Room('Classroom', 1, 0.18, 250)
        cls.supply_room = room.Room('Supply Closet', 3, 0.06, 30)
        cls.swegon_12 = air_handler.AirHandler('Swegon Gold RX 12', 1000, 35)
        cls.swegon_14 = air_handler.AirHandler('Swegon Gold RX 14', 2000, 50)
        cls.hvac_bank.add_air_handler(cls.swegon_12)
        cls.hvac_bank.add_air_handler(cls.swegon_14)
        cls.hvac_bank.add_room(cls.art_room)
        cls.hvac_bank.add_room(cls.class_room)
        cls.hvac_bank.add_room(cls.supply_room)
        cls.hvac_bank.add_air_handler(cls.swegon_12)
        cls.hvac_bank.add_air_handler(cls.swegon_14)

        @classmethod
        def tearDownClass(cls):
            print('tearDownClass()')

        def setUp(self):
            print('setUp()')

        def tearDown(self):
            print('tearDown()')

    def remove_room(self):
        print('test remove room')

        self.hvac_bank.remove_room(self.art_room)
        for()

        self.assertEqual(True, False)  # add assertion here


if __name__ == '__main__':
    unittest.main()
