# room: building has instances of room
# hvac_systems has instances of room types

import math


class Room:

    def __init__(self, name, category, density, square_footage=0.0):
        self.name = name
        self.category = category
        self.density = density
        self.square_footage = square_footage
        self.capacity = int(math.floor((self.square_footage / 1000.0) * self.density))

    def __eq__(self, other):
        return self.name == other.name and self.square_footage == other.square_footage

    def to_string(self):
        s = "Name: " + self.name + ", Category: " + self.category + ", Density: " + self.density
        return s

    def get_square_footage(self):
        return self.square_footage

    def set_square_footage(self, square_footage):
        self.square_footage = square_footage

    def calc_req_cfm(self):
        cfm_person = 0
        cfm_sqft = 0

        if self.category == 1:
            cfm_person = 5.0 * self.capacity
            cfm_sqft = 0.06 * self.capacity
        if self.category == 2:
            cfm_person = 7.5 * self.capacity
            cfm_sqft = 0.12 * self.capacity
        if self.category == 3:
            cfm_person = 10.0 * self.capacity
            cfm_sqft = 0.18 * self.capacity

        if int(math.ceil(cfm_person)) < int(math.ceil(cfm_sqft)):
            return cfm_sqft
        else:
            return cfm_person
