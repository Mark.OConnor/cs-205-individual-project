import air_handler
import room
import building
import hvac_bank

hvac_systems = list()


def get_valid_input(prompt, error, high, low):
    choice = input(prompt)
    choice = int(choice)
    while choice > high or choice < low:
        print(error)
        choice = input(prompt)
        choice = int(choice)
    return choice


def hvac_edit_menu(hvac_database):

    choice = get_valid_input("Enter what you would like to do to the "
                             + hvac_database.get_name() +
                             "database\n"
                             "(1) Add a building\n"
                             "(2) Remove a building\n"
                             "(3) Edit a building\n"
                             "(4) Add a room\n"
                             "(5) Remove a room\n "
                             "(6) Add a air handler\n"
                             "(7) Remove a handler\n",
                             "Invalid choice. Enter 0 - 7",
                             1, 7)
    if choice == 1:
        name = input("Enter building name to add: ")
        hvac_database.add_building(building.Building(name))
    if choice == 2:
        name = input("Enter building name to remove: ")
        hvac_database.remove_building(building.Building(name))


def hvac_menu():
    global hvac_systems
    last_num = len(hvac_systems)
    for h in range(last_num):
        print(h, ": " + hvac_systems[h].get_name() + "\n")
    choice = get_valid_input("Please choose the database you would like to edit from above"
                             "by entering the number next to it", "Invalid Choice. Enter 0 - " + str(last_num),
                             last_num, 0)
    hvac_edit_menu(hvac_systems[choice])


def main_menu():
    global hvac_systems
    choice = get_valid_input("Welcome to the HVAC Design Systems\n"
                             "Please Enter and option\n"
                             "(1) Create new HVAC Database\n"
                             "(2) Edit Existing HVAC Database\n"
                             "(3) Exit\n", "Invalid choice. Enter '1', '2', or '3'", 3, 1)
    choice = int(choice)
    if choice == 1:
        name = input("Enter name of new HVAC Database: ")
        hvac_systems.append(hvac_bank.HvacBank(name))
        main_menu()
    elif choice == 2:
        hvac_menu()
    else:
        return


def main():
    main_menu()
    print("continue")


main()
