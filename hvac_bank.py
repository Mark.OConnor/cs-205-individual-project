
class HvacBank:
    s_hvac_bank = None

    @classmethod
    def get(self):
        if self.s_hvac_bank is None:
            self.s_hvac_bank = HvacBank()
        return self.s_hvac_bank

    def __init__(self, name):
        self.name = name
        self.air_handlers = list()
        self.rooms = list()
        self.buildings = list()

    def get_name(self):
        return self.name

    def __eq__(self, other):
        return self.name == other.name

    def find_room(self, room):
        for r in self.rooms:
            if self.rooms[r] == room:
                return True
        return False

    def add_room(self, room):
        if self.find_room(room):
            print("This room has already been added to this library")
        elif room.square_footage != 0.0:
            print("This room is specific to a building, \n"
                  "Please set the square footage to 0.0 before adding it to the library")
        else:
            self.rooms.append(room)

    def remove_room(self, room):
        if not self.find_room(room):
            print("This room is not in the library, thus cannot be removed \n")
        else:
            self.rooms.remove(room)

    def show_rooms(self):
        for r in self.rooms:
            print(r.to_string)

    def find_air_handler(self, air_handler):
        for a in self.air_handlers:
            if self.air_handlers[a] == air_handler:
                return True
        return False

    def add_air_handler(self, air_handler):
        if self.find_air_handler(air_handler):
            print("This air handler has already been added to this library")
        else:
            self.air_handlers.append(air_handler)

    def remove_air_handler(self, air_handler):
        if not self.find_air_handler(air_handler):
            print("This air handler is not in the library, thus cannot be removed \n")
        else:
            self.air_handlers.remove(air_handler)

    def show_air_handlers(self):
        for a in self.air_handlers:
            print(a.to_string)

    def find_building(self, building):
        for a in self.buildings:
            if self.buildings[a] == building:
                return True
        return False

    def add_building(self, building):
        if self.find_building(building):
            print("This building has already been added to this library")
        else:
            self.buildings.append(building)

    def remove_building(self, building):
        if not self.find_building(building):
            print("This building is not in the library, thus cannot be removed \n")
        else:
            self.buildings.remove(building)

    def show_buildings(self):
        for b in self.buildings:
            print(b.to_string)
